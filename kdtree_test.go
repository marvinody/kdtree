package kdtree

import (
	"math/rand"
	"testing"
)

func TestFastMod(t *testing.T) {
	tests := [][]int{
		[]int{3, 2},
		[]int{0, 2},
		[]int{5, 4},
		[]int{-1, 2},
	}
	for _, arr := range tests {
		left, right := arr[0], arr[1]
		expected, actual := left%right, fastMod(left, right)
		if expected < 0 {
			expected = -1 * expected
		}
		if expected != actual {
			t.Fatalf("Expected %d, got %d for %d %% %d", expected, actual, left, right)
		}
	}
}

func checkBuild(t *testing.T, node *kDTreeNode, expected int, ints ...int) {
	for _, idx := range ints {
		if idx == 0 {
			node = node.leftChild
		} else if idx == 1 {
			node = node.rightChild
		}
	}
	if node.locationIdx != expected {
		//if tree.location != expected {
		t.Fatalf("Expected %v at location %v but got %v", expected, ints, node.locationIdx)
	}
}

func TestEmptyTreeError(t *testing.T) {
	data := [][]float64{}
	_, err := BatchCreate(data)
	if err == nil {
		t.Errorf("Error returned was nil instead of real error")
	} else if err.Error() != "Need at least one point in data to build a tree" {
		t.Errorf("Wrong error given")
	}
}

func TestTreeBuild(t *testing.T) {
	data := [][]float64{
		[]float64{2, 3},
		[]float64{5, 4},
		[]float64{9, 6},
		[]float64{4, 7},
		[]float64{8, 1},
		[]float64{7, 2},
	}

	tree, _ := BatchCreate(data)
	/*
		tree should be
						  (7,2)
						/	 			\
					(5,4) 			(9,6)
				/      \ 		   /
			(2,3)   (4,7)  (8,1)
	*/
	checkBuild(t, tree.root, 5)       // should be root
	checkBuild(t, tree.root, 1, 0)    // should be (5,4)
	checkBuild(t, tree.root, 0, 0, 0) // should be (2,3)
	checkBuild(t, tree.root, 3, 0, 1) // should be (4,7)

	checkBuild(t, tree.root, 2, 1)    // should be (9,6)
	checkBuild(t, tree.root, 4, 1, 0) // should be (8,1)

}

func TestDupeDataTreeBuild(t *testing.T) {
	data := [][]float64{
		[]float64{2, 3}, // 0
		[]float64{2, 3}, // 1
		[]float64{2, 3}, // 2
		[]float64{3, 4}, // 3
		[]float64{5, 5}, // 4
		[]float64{4, 5}, // 5
		[]float64{4, 5}, // 6
	}
	tree, _ := BatchCreate(data)
	checkBuild(t, tree.root, 3)
	checkBuild(t, tree.root, 2, 0)
	checkBuild(t, tree.root, 1, 0, 0)
	checkBuild(t, tree.root, 0, 0, 0, 0)
	checkBuild(t, tree.root, 4, 1)
	checkBuild(t, tree.root, 6, 1, 0)
	checkBuild(t, tree.root, 5, 1, 0, 0)
}

func checkMatch(t *testing.T, expected, actual []float64) {
	if !tupleEquals(expected, actual) {
		//if tree.location != expected {
		t.Fatalf("Expected %v but got %v", expected, actual)
	}
}

func TestInsertError(t *testing.T) {
	tree, _ := BatchCreate([][]float64{
		[]float64{0, 0},
	})
	_, err := tree.Insert()
	if err == nil {
		t.Error("Should have returned error on 0 tuple insert")
	}
}

/*
func TestDelete(t *testing.T) {
	tree, _ := BatchCreate([][]float64{
		[]float64{2, 3},
		[]float64{5, 4},
		[]float64{9, 6},
		[]float64{4, 7},
		[]float64{8, 1},
		[]float64{7, 2},
	})

	/*
		tree should be
							(7,2)         x
						/	 			\
					(5,4) 			(9,6) y
				/      \ 		   /
			(2,3)   (4,7)  (8,1)  x

	fmt.Println(tree.String())
	tree.DeleteByTuple([]float64{2, 3})
	fmt.Println(tree.String())

}*/

func TestString(t *testing.T) {
	tree, _ := BatchCreate([][]float64{
		[]float64{-1, -1},
		[]float64{0, 0},
		[]float64{1, 1},
	})
	s := tree.String()
	expected :=
		`└── 1
    ├── 0
    └── 2` + "\n"
	if s != expected {
		t.Errorf("Expected \n%s but got \n%s", expected, s)
	}
}

func TestRootInsert(t *testing.T) {
	tree := KDTree{}
	tree.Insert([]float64{0, 0})
	if tree.root.locationIdx != 0 {
		t.Fatal("Root isn't indexed correctly")
	}
	if len(tree.data) != 1 {
		t.Fatal("Data should only be length 1 with 1 insert done")
	}
	if !tupleEquals([]float64{0, 0}, tree.data[0]) {
		t.Fatal("Expected tuple inserted doesn't match actual")
	}
}

func TestInsert(t *testing.T) {
	tree, _ := BatchCreate([][]float64{
		[]float64{0, 0},
	})
	stuff := []struct {
		tuple    []float64
		treeInds []int
	}{
		{[]float64{1, 1}, []int{1}},
		{[]float64{-1, -1}, []int{0}},
		{[]float64{2, 2}, []int{1, 1}},
		{[]float64{2, 3}, []int{1, 1, 0}},
		{[]float64{3, 2}, []int{1, 1, 1}},
		{[]float64{3, 3}, []int{1, 1, 1, 1}},
	}

	for idx, entry := range stuff {
		_, err := tree.Insert(entry.tuple)
		if err != nil {
			t.Error("Should not have returned error on insert")
		}
		// check incrementally to make sure it's been placed correctly
		checkBuild(t, tree.root, idx+1, entry.treeInds...)
	}
	// check after building all too, just to make sure nothing changed
	for idx, entry := range stuff {
		checkBuild(t, tree.root, idx+1, entry.treeInds...)
	}
}
func TestInsert2(t *testing.T) {
	tree := KDTree{}
	stuff := []struct {
		tuple    []float64
		treeInds []int
	}{
		{[]float64{0, 0}, []int{}},
		{[]float64{-1, 1}, []int{0}},
		{[]float64{-2, -1}, []int{0, 0}},
	}
	for idx, entry := range stuff {
		_, err := tree.Insert(entry.tuple)
		if err != nil {
			t.Error("Should not have returned error on insert")
		}
		// check incrementally to make sure it's been placed correctly
		checkBuild(t, tree.root, idx, entry.treeInds...)
	}
	// check after building all too, just to make sure nothing changed
	for idx, entry := range stuff {
		checkBuild(t, tree.root, idx, entry.treeInds...)
	}
}

func TestInsertBalance(t *testing.T) {
	tree, _ := BatchCreate([][]float64{
		[]float64{0, 0},
	})
	stuff := []struct {
		tuple    []float64
		treeInds []int
	}{
		{[]float64{1, 1}, []int{0}},
		{[]float64{-1, -1}, []int{0, 0, 0}},
		{[]float64{2, 2}, []int{0, 1}},
		{[]float64{2, 3}, []int{}},
		{[]float64{3, 2}, []int{1, 0}},
		{[]float64{3, 3}, []int{1}},
	}

	for _, entry := range stuff {
		_, err := tree.Insert(entry.tuple)
		if err != nil {
			t.Error("Should not have returned error on insert")
		}
	}
	tree.Balance()
	// check after building all too, just to make sure nothing changed
	for idx, entry := range stuff {
		checkBuild(t, tree.root, idx+1, entry.treeInds...)
	}
}

func TestNearestNeighbor(t *testing.T) {
	data := [][]float64{
		[]float64{2, 3},
		[]float64{5, 4},
		[]float64{9, 6},
		[]float64{4, 7},
		[]float64{8, 1},
		[]float64{7, 2},
	}

	tree, _ := BatchCreate(data)
	checkMatch(t, data[0], tree.FindNearestNeighborTuple([]float64{2.001, 3.001}))
	checkMatch(t, data[1], tree.FindNearestNeighborTuple([]float64{5.5, 4}))
	checkMatch(t, data[2], tree.FindNearestNeighborTuple([]float64{100, 100}))
	checkMatch(t, data[3], tree.FindNearestNeighborTuple([]float64{3, 6.001}))
	checkMatch(t, data[4], tree.FindNearestNeighborTuple([]float64{8.9, 0}))
	// next originally written to have (7,2) as ans. but apparently (8,1) is closer
	// spent a while debugging where algo was wrong until I realized I just can't pick good test cases almost ever
	checkMatch(t, data[4], tree.FindNearestNeighborTuple([]float64{7, -1}))
	// added to make sure root is being checked
	checkMatch(t, data[5], tree.FindNearestNeighborTuple([]float64{7, 1.5}))

}

func TestNearestNNeighborsWithinDistance(t *testing.T) {
	data := [][]float64{
		[]float64{2, 3},
		[]float64{5, 4},
		[]float64{9, 6},
		[]float64{4, 7},
		[]float64{8, 1},
		[]float64{7, 2},
	}

	tree, _ := BatchCreate(data)
	testCases := []struct {
		tuple        []float64
		n            int
		minDist      float64
		expectedInds []int
	}{
		{
			[]float64{7, 1.5},
			2, // test exact amount
			2.0,
			[]int{5, 4},
		},
		{
			[]float64{7, 1.5},
			3, // more but distance limits
			2.0,
			[]int{5, 4},
		},
		{
			[]float64{7, 1.5},
			1, // multiple but only 1 allowed
			2.0,
			[]int{5},
		},
		{
			[]float64{7, 1},
			1,
			1,
			[]int{5},
		},
		{
			[]float64{7, 0},
			1,
			2,
			[]int{5},
		},
	}
	for testIdx, stuff := range testCases {
		tuple := stuff.tuple
		n, minDist := stuff.n, stuff.minDist
		neighbors := tree.FindNearestNNeighborsWithinDistanceInds(tuple, n, minDist)
		expectedInds := stuff.expectedInds
		if len(neighbors) != len(expectedInds) {
			t.Fatalf("Expected an array with %d neighbors, but got %d, testIdx:%d\n%v", len(expectedInds), len(neighbors), testIdx, neighbors)
		}
		for idx := range expectedInds {
			if expectedInds[idx] != neighbors[idx] {
				t.Fatalf("Expected [%d] instead of [%d] in position: %d, testIdx:%d", expectedInds[idx], neighbors[idx], idx, testIdx)
			}
		}
	}

}

func TestAllNeighborsIndsWithinDistance(t *testing.T) {
	data := [][]float64{
		[]float64{2, 3},
		[]float64{5, 4},
		[]float64{9, 6},
		[]float64{4, 7},
		[]float64{8, 1},
		[]float64{7, 2},
	}

	tree, _ := BatchCreate(data)
	testCases := []struct {
		tuple        []float64
		dist         float64
		expectedInds []int
	}{
		{
			[]float64{7, 1.5},
			2.0,
			[]int{5, 4}, // general
		},
		{
			[]float64{5, 5},
			0.5,
			[]int{}, // small dist
		},
		{
			[]float64{5, 5},
			10,
			[]int{1, 3, 5, 0, 2, 4},
		},
		{ // some more general cases follow
			[]float64{9, 7},
			1,
			[]int{2},
		},
		{
			[]float64{9, 7},
			5,
			[]int{2, 1, 3},
		},
		{
			[]float64{2, 2},
			1,
			[]int{0},
		},
	}
	for testIdx, stuff := range testCases {
		tuple := stuff.tuple
		dist := stuff.dist
		neighbors := tree.FindAllNeighborsIndsWithinDistance(tuple, dist)
		expectedInds := stuff.expectedInds
		if len(neighbors) != len(expectedInds) {
			t.Fatalf("Expected an array with %d neighbors, but got %d, testIdx:%d\n%v", len(expectedInds), len(neighbors), testIdx, neighbors)
		}
		for idx := range expectedInds {
			if expectedInds[idx] != neighbors[idx] {
				t.Fatalf("Expected [%d] instead of [%d] in position: %d, testIdx:%d", expectedInds[idx], neighbors[idx], idx, testIdx)
			}
		}
	}

}

func TestNearestNNeighbors(t *testing.T) {
	data := [][]float64{
		[]float64{2, 3},
		[]float64{5, 4},
		[]float64{9, 6},
		[]float64{4, 7},
		[]float64{8, 1},
		[]float64{7, 2},
	}

	tree, _ := BatchCreate(data)
	n := 3
	neighbors := tree.FindNearestNNeighborsInds([]float64{6, 1}, n)
	if len(neighbors) != n {
		t.Fatalf("Exepected an array with %d neighbors, but got %d", n, len(neighbors))
	}
	// closest will be 7,2   8,1    5,4
	//                 5      4      1
	checkMatch(t, data[5], data[neighbors[0]])
	checkMatch(t, data[4], data[neighbors[1]])
	checkMatch(t, data[1], data[neighbors[2]])
}

func Test100KNN(t *testing.T) {
	n := 100
	tuple := []float64{0, 0}
	neighbors := 3
	l := make([][]float64, n)
	for idx := range l {
		l[idx] = []float64{float64(idx), float64(idx)}
	}
	tree, _ := BatchCreate(l)
	tree.FindNearestNNeighborsInds(tuple, neighbors)
}

/*
func TestMinMax(t *testing.T) {
	data := [][]float64{
		[]float64{2, 3},
		[]float64{5, 4},
		[]float64{9, 6},
		[]float64{4, 7},
		[]float64{8, 1},
		[]float64{7, 2},
	}

	tree, _ := BatchCreate(data)
	/*
		tree should be
						  (7,2)         x
						/	 			\
					(5,4) 			(9,6) y
				/      \ 		   /
			(2,3)   (4,7)  (8,1)  x

	// mins
	minXNode, minXDepth := tree.getMin(tree.root, 0, 0)
	expectedMinXTuple, expectedMinXDepth := data[0], 0
	if !tupleEquals(expectedMinXTuple, tree.data[minXNode.locationIdx]) || expectedMinXDepth != minXDepth {
		t.Fatalf("%s failed, expected to get %v, %d but got %v, %d", "MinX Find", expectedMinXTuple, expectedMinXDepth, tree.data[minXNode.locationIdx], minXDepth)
	}
	minYNode, minYDepth := tree.getMin(tree.root, 1, 0)
	expectedMinYTuple, expectedMinYDepth := data[4], 0
	if !tupleEquals(expectedMinYTuple, tree.data[minYNode.locationIdx]) || expectedMinYDepth != minYDepth {
		t.Fatalf("%s failed, expected to get %v, %d but got %v, %d", "MinY Find", expectedMinYTuple, expectedMinYDepth, tree.data[minYNode.locationIdx], minYDepth)
	}
	// maxs
	maxXNode, maxXDepth := tree.getMax(tree.root, 0, 0)
	expectedMaxXTuple, expectedMaxXDepth := data[2], 1
	if !tupleEquals(expectedMaxXTuple, tree.data[maxXNode.locationIdx]) || expectedMaxXDepth != maxXDepth {
		t.Fatalf("%s failed, expected to get %v, %d but got %v, %d", "MaxX Find", expectedMaxXTuple, expectedMaxXDepth, tree.data[maxXNode.locationIdx], maxXDepth)
	}
	maxYNode, maxYDepth := tree.getMax(tree.root, 1, 0)
	expectedMaxYTuple, expectedMaxYDepth := data[3], 0
	if !tupleEquals(expectedMaxYTuple, tree.data[maxYNode.locationIdx]) || expectedMaxYDepth != maxYDepth {
		t.Fatalf("%s failed, expected to get %v, %d but got %v, %d", "MaxY Find", expectedMaxYTuple, expectedMaxYDepth, tree.data[maxYNode.locationIdx], maxYDepth)
	}

}
*/

func TestRandom(t *testing.T) {
	// clump n points around [0,1) on both x and y
	// introduce 3 points far away and then search for them
	n := 10000
	l := make([][]float64, n+3)
	for i := 0; i < n; i += 1 {
		l[i] = []float64{rand.Float64(), rand.Float64()}
	}
	l[n+0] = []float64{10, 10}
	l[n+1] = []float64{11, 11}
	l[n+2] = []float64{12, 12}
	tree, _ := BatchCreate(l)
	neighbors := tree.FindNearestNNeighborsInds([]float64{100, 100}, 3)
	if len(neighbors) == n {
		t.Fatalf("Exepected an array with %d neighbors, but got %d", n, len(neighbors))
	}
	// closest will be 7,2   8,1    5,4
	//                 5      4      1
	checkMatch(t, l[n+2], l[neighbors[0]])
	checkMatch(t, l[n+1], l[neighbors[1]])
	checkMatch(t, l[n+0], l[neighbors[2]])
}

func benchmarkHelper(b *testing.B, n int, tuple []float64, neighbors int) {
	l := make([][]float64, n)
	for idx := range l {
		l[idx] = []float64{float64(idx), float64(idx)}
	}
	tree, _ := BatchCreate(l)
	b.ResetTimer()
	for n := 0; n < b.N; n += 1 {
		tree.FindNearestNNeighborsInds(tuple, neighbors)
	}

}

func Benchmark100(b *testing.B) {
	benchmarkTuple := []float64{0, 0}
	const benchmarkNeighbors = 3
	benchmarkHelper(b, 100, benchmarkTuple, benchmarkNeighbors)
}

func Benchmark1000(b *testing.B) {
	benchmarkTuple := []float64{0, 0}
	const benchmarkNeighbors = 3
	benchmarkHelper(b, 1000, benchmarkTuple, benchmarkNeighbors)
}

func Benchmark10000(b *testing.B) {
	benchmarkTuple := []float64{0, 0}
	const benchmarkNeighbors = 3
	benchmarkHelper(b, 10000, benchmarkTuple, benchmarkNeighbors)
}

func Benchmark100000(b *testing.B) {
	benchmarkTuple := []float64{50000, 50000}
	const benchmarkNeighbors = 3
	benchmarkHelper(b, 100000, benchmarkTuple, benchmarkNeighbors)
}
func Benchmark1000000(b *testing.B) {
	benchmarkTuple := []float64{500000, 500000}
	const benchmarkNeighbors = 5
	benchmarkHelper(b, 1000000, benchmarkTuple, benchmarkNeighbors)
}
func Benchmark10000000(b *testing.B) {
	benchmarkTuple := []float64{5000000, 5000000}
	const benchmarkNeighbors = 5
	benchmarkHelper(b, 10000000, benchmarkTuple, benchmarkNeighbors)
}
