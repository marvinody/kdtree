package kdtree

import (
	"fmt"
	"math"
	"sort"

	"bitbucket.org/marvinody/treeprint"
)

// Probably not faster in go because can't inline stuff but idkk
func fastMod(k, d int) int {
	if k >= d {
		k = k - d
	} else if k < 0 { // handle neg
		k = k + d
	}
	return k
}

// KDTree is the main struct for the package
type KDTree struct {
	root    *kDTreeNode
	data    [][]float64
	deleted []int
}

func (tree *KDTree) Size() int {
	return len(tree.data)
}

func (tree *KDTree) Get(idx int) []float64 {
	return tree.data[idx]
}

type kDTreeNode struct {
	locationIdx int
	leftChild   *kDTreeNode
	rightChild  *kDTreeNode
	parent      *kDTreeNode // dont think I need but I'm gonna keep
}

func (node *kDTreeNode) isLeaf() bool {
	return node.leftChild == nil && node.rightChild == nil
}
func (node *kDTreeNode) GetChildren() []treeprint.Tree {
	list := make([]treeprint.Tree, 0, 2)
	if node.leftChild != nil {
		list = append(list, node.leftChild)
	}
	if node.rightChild != nil {
		list = append(list, node.rightChild)
	}
	return list
}

func (node *kDTreeNode) StringValue() string {
	return fmt.Sprintf("%d", node.locationIdx)
}

func (tree *KDTree) String() string {
	return treeprint.Simple(tree.root)
}

// NoPointsError is returned when 0 points are passed in to something that needs them
type NoPointsError struct {
}

func (e NoPointsError) Error() string {
	return "Need at least one point in data to build a tree"
}

// BatchCreate will create a balanced kdtree with given data
// Later inserts/deletes will probably unbalance it
// This is the best way to create it if you know all your data before
func BatchCreate(data [][]float64) (*KDTree, error) {
	if len(data) == 0 {
		return nil, NoPointsError{}
	}

	dupe := make([][]float64, len(data))
	copy(dupe, data)

	tree := &KDTree{}
	tree.data = dupe // hold a copy so if the user changes orig, we're good
	tree.Balance()
	tree.deleted = []int{} // initialize for any deletes later
	return tree, nil
}

// Balance will iterate over all the data and reconstruct the tree
// Do not call unless you have inserted a lot of points
func (tree *KDTree) Balance() {
	// make an array idx instead of carrying data everywhere
	// also makes it able to have dupe points in data
	idxs := make([]int, len(tree.data))
	for idx := range tree.data {
		idxs[idx] = idx
	}
	tree.root = kDTreeHelper(tree.data, idxs, 0)
}

func kDTreeHelper(data [][]float64, idxs []int, depth int) *kDTreeNode {
	if len(idxs) == 0 {
		return nil
	}
	p := fastMod(depth, len(data[idxs[0]]))
	sortTupleSliceWithPriority(data, idxs, p)
	tree := &kDTreeNode{}
	median := len(idxs) / 2
	// we want all the equal values to be on the left children
	for i := median; i+1 < len(idxs) && (data[idxs[i]][p] == data[idxs[i+1]][p]); i, median = i+1, median+1 {
		// so just iterate over until unequal values or end of thing
	}
	tree.locationIdx = idxs[median]
	tree.leftChild = kDTreeHelper(data, idxs[:median], p+1)
	if tree.leftChild != nil {
		tree.leftChild.parent = tree
	}
	tree.rightChild = kDTreeHelper(data, idxs[median+1:], p+1)
	if tree.rightChild != nil {
		tree.rightChild.parent = tree
	}
	return tree
}

func sortTupleSliceWithPriority(data [][]float64, idxs []int, p int) {
	sort.SliceStable(idxs, func(i, j int) bool {
		a, b := data[idxs[i]], data[idxs[j]]
		dims := len(a)
		for k := 0; k < dims; k += 1 {
			dim := fastMod(p+k, dims) // get the right dimension we want for iteration
			// true if it comes before
			if a[dim] < b[dim] {
				return true
			} else if a[dim] > b[dim] {
				return false
			}

		}
		return false // same coords? prob won't happen
	})

}

// Insert will insert given points into the tree and returns index of the last point entered
// Returned idx is useful if you're going to do NN and want to just have ints as keys
// Save it in a map and then later when you do nearest neighbor, you can pull whatever assoc. data structure in the map
func (tree *KDTree) Insert(data ...[]float64) (int, error) {
	if len(data) == 0 {
		return 0, NoPointsError{}
	}
	for _, tuple := range data {
		tree.data = append(tree.data, tuple)
		if tree.root == nil {
			tree.root = &kDTreeNode{locationIdx: len(tree.data) - 1}
			continue
		}
		tree.root.singleInsert(tree.data, len(tree.data)-1, 0)
	}
	return len(tree.data) - 1, nil
}

// will unbalance the tree and insert into an appropriate spot in the tree
func (node *kDTreeNode) singleInsert(data [][]float64, idx, depth int) {
	p := fastMod(depth, len(data[idx]))
	loc := data[node.locationIdx]
	tuple := data[idx]
	if tuple[p] <= loc[p] {
		if node.leftChild == nil {
			node.leftChild = &kDTreeNode{locationIdx: idx}
			node.leftChild.parent = node
		} else {
			node.leftChild.singleInsert(data, idx, p+1)
		}
	} else {
		if node.rightChild == nil {
			node.rightChild = &kDTreeNode{locationIdx: idx}
			node.rightChild.parent = node
		} else {
			node.rightChild.singleInsert(data, idx, p+1)
		}
	}

}

type idxDist struct {
	idx  int
	dist float64
}

// FindNearestNNeighborsInds finds n neighbors closest to tuple and returns their indexes
func (tree *KDTree) FindNearestNNeighborsInds(tuple []float64, n int) []int {
	list := tree.findNearestNNeighborsIndsHelper(tree.root, tuple, 0, n, n, math.MaxFloat64)
	idxs := make([]int, len(list))
	for idx, s := range list {
		idxs[idx] = s.idx
	}
	return idxs
}

// FindNearestNNeighborsTuples finds n neighbors closest to tuple and returns copies of the tuples
func (tree *KDTree) FindNearestNNeighborsTuples(tuple []float64, n int) [][]float64 {
	return [][]float64{}
	list := tree.findNearestNNeighborsIndsHelper(tree.root, tuple, 0, n, n, math.MaxFloat64)
	data := make([][]float64, len(list)) // copy the tuples over to prevent user from changing their value
	for idx, entry := range list {
		tuple := tree.data[entry.idx]
		data[idx] = make([]float64, len(tuple))
		copy(data[idx], tuple)

	}
	return data
}

// Given a node, will find the n closest inds to given tuple. Returns an intermediary struct to hold distance to speed up calc
func (tree *KDTree) findNearestNNeighborsIndsHelper(node *kDTreeNode, tuple []float64, depth int, n, needed int, maxDist float64) []idxDist {
	if node == nil {
		return []idxDist{}
	}
	k := fastMod(depth, len(tuple))
	loc := tree.Get(node.locationIdx)
	dist := distanceSq(tuple, loc)

	list := []idxDist{idxDist{node.locationIdx, dist}}
	merge := func(l, r []idxDist) []idxDist {
		o := make([]idxDist, len(l)+len(r))
		copy(o[:len(l)], l)
		copy(o[len(l):], r)
		sort.Slice(o, func(i, j int) bool {
			return o[i].dist < o[j].dist
		})
		return o
	}
	min := func(a, b int) int {
		if a < b {
			return a
		}
		return b
	}
	limit := func(list []idxDist, n int) []idxDist {
		out := make([]idxDist, min(len(list), n))
		copy(out, list)
		return out
	}
	lastOrDefault := func(list []idxDist, def float64) float64 {
		if len(list) == 0 {
			return def
		}
		return list[len(list)-1].dist
	}
	if tuple[k] <= loc[k] { // if we fall into the plane, go check it
		// check the left then the right
		newList := tree.findNearestNNeighborsIndsHelper(node.leftChild, tuple, k+1, n, needed, maxDist)
		list = limit(merge(list, newList), n)          // merge two lists then only take n stuff
		maxDist = lastOrDefault(list, math.MaxFloat64) // get the greatest distance (because we want anything less) or max if empty
		if tuple[k]+maxDist > loc[k] {                 // if the best distance (so far) is still kinda bad compared to best possible
			newList := tree.findNearestNNeighborsIndsHelper(node.rightChild, tuple, k+1, n, needed, maxDist)
			list = limit(merge(list, newList), n)          // merge two lists then only take n stuff
			maxDist = lastOrDefault(list, math.MaxFloat64) // get the greatest distance (because we want anything less) or max if empty

		}
	} else if tuple[k] > loc[k] { // and here, check the right then the left
		newList := tree.findNearestNNeighborsIndsHelper(node.rightChild, tuple, k+1, n, needed, maxDist)
		list = limit(merge(list, newList), n)          // merge two lists then only take n stuff
		maxDist = lastOrDefault(list, math.MaxFloat64) // get the greatest distance (because we want anything less) or max if empty
		if tuple[k]-maxDist <= loc[k] {                // if the best distance (so far) is still kinda bad compared to best possible
			newList := tree.findNearestNNeighborsIndsHelper(node.leftChild, tuple, k+1, n, needed, maxDist)
			list = limit(merge(list, newList), n)          // merge two lists then only take n stuff
			maxDist = lastOrDefault(list, math.MaxFloat64) // get the greatest distance (because we want anything less) or max if empty

		}
	}
	return list

}

func (tree *KDTree) FindNearestNNeighborsWithinDistanceInds(tuple []float64, n int, minDist float64) []int {
	list := tree.findNearestNNeighborsIndsWithinDistanceHelper(tree.root, tuple, 0, n, n, minDist*minDist, math.MaxFloat64)
	idxs := make([]int, len(list))
	for idx, s := range list {
		idxs[idx] = s.idx
	}
	return idxs
}

// Given a node, will find the n closest inds to given tuple. Returns an intermediary struct to hold distance to speed up calc
func (tree *KDTree) findNearestNNeighborsIndsWithinDistanceHelper(node *kDTreeNode, tuple []float64, depth int, n, needed int, minDist, maxDist float64) []idxDist {
	if node == nil {
		return []idxDist{}
	}
	k := fastMod(depth, len(tuple))
	loc := tree.Get(node.locationIdx)
	dist := distanceSq(tuple, loc)

	list := []idxDist{}
	if dist <= minDist {
		list = append(list, idxDist{node.locationIdx, dist})
	}
	merge := func(l, r []idxDist) []idxDist {
		o := make([]idxDist, len(l)+len(r))
		copy(o[:len(l)], l)
		copy(o[len(l):], r)
		sort.Slice(o, func(i, j int) bool {
			return o[i].dist < o[j].dist
		})
		return o
	}
	min := func(a, b int) int {
		if a < b {
			return a
		}
		return b
	}
	limit := func(list []idxDist, n int) []idxDist {
		out := make([]idxDist, min(len(list), n))
		copy(out, list)
		return out
	}
	lastOrDefault := func(list []idxDist, def float64) float64 {
		if len(list) == 0 {
			return def
		}
		return list[len(list)-1].dist
	}
	if len(list) < needed || (tuple[k] <= loc[k] && loc[k]-tuple[k] < minDist) { // if we fall into the plane, go check it
		// check the left then the right
		newList := tree.findNearestNNeighborsIndsWithinDistanceHelper(node.leftChild, tuple, k+1, n, needed-1, minDist, maxDist)
		list = limit(merge(list, newList), n)                                               // merge two lists then only take n stuff
		maxDist = lastOrDefault(list, math.MaxFloat64)                                      // get the greatest distance (because we want anything less) or max if empty
		if len(list) < needed || (tuple[k]+maxDist > loc[k] && loc[k]-tuple[k] < minDist) { // if the best distance (so far) is still kinda bad compared to best possible
			newList := tree.findNearestNNeighborsIndsWithinDistanceHelper(node.rightChild, tuple, k+1, n, needed-1, minDist, maxDist)
			list = limit(merge(list, newList), n)          // merge two lists then only take n stuff
			maxDist = lastOrDefault(list, math.MaxFloat64) // get the greatest distance (because we want anything less) or max if empty
		}
	} else if len(list) < needed || (tuple[k] > loc[k] && tuple[k]-loc[k] < minDist) { // and here, check the right then the left
		newList := tree.findNearestNNeighborsIndsWithinDistanceHelper(node.rightChild, tuple, k+1, n, needed-1, minDist, maxDist)
		list = limit(merge(list, newList), n)                                                // merge two lists then only take n stuff
		maxDist = lastOrDefault(list, math.MaxFloat64)                                       // get the greatest distance (because we want anything less) or max if empty
		if len(list) < needed || (tuple[k]-maxDist <= loc[k] && tuple[k]-loc[k] < minDist) { // if the best distance (so far) is still kinda bad compared to best possible
			newList := tree.findNearestNNeighborsIndsWithinDistanceHelper(node.leftChild, tuple, k+1, n, needed-1, minDist, maxDist)
			list = limit(merge(list, newList), n)          // merge two lists then only take n stuff
			maxDist = lastOrDefault(list, math.MaxFloat64) // get the greatest distance (because we want anything less) or max if empty
		}
	}

	return list

}

func (tree *KDTree) FindAllNeighborsIndsWithinDistance(tuple []float64, dist float64) []int {
	list := tree.findAllNeighborsIndsWithinDistanceHelper(tree.root, tuple, 0, dist*dist, math.MaxFloat64)
	idxs := make([]int, len(list))
	for idx, s := range list {
		idxs[idx] = s.idx
	}
	return idxs
}

// Given a node, will find all the closest inds to given tuple. Returns an intermediary struct to hold distance to speed up calc
func (tree *KDTree) findAllNeighborsIndsWithinDistanceHelper(node *kDTreeNode, tuple []float64, depth int, minDist, maxDist float64) []idxDist {
	if node == nil {
		return []idxDist{}
	}
	k := fastMod(depth, len(tuple))
	loc := tree.Get(node.locationIdx)
	dist := distanceSq(tuple, loc)

	list := []idxDist{}

	merge := func(l, r []idxDist) []idxDist {
		o := make([]idxDist, len(l)+len(r))
		copy(o[:len(l)], l)
		copy(o[len(l):], r)
		sort.Slice(o, func(i, j int) bool {
			return o[i].dist < o[j].dist
		})
		return o
	}

	if dist <= minDist {
		list = append(list, idxDist{node.locationIdx, dist})
		leftList := tree.findAllNeighborsIndsWithinDistanceHelper(node.leftChild, tuple, k+1, minDist, maxDist)
		rightList := tree.findAllNeighborsIndsWithinDistanceHelper(node.rightChild, tuple, k+1, minDist, maxDist)
		list = merge(list, leftList) // merge two lists
		list = merge(list, rightList)
	} else {
		if tuple[k]-minDist < loc[k] {
			newList := tree.findAllNeighborsIndsWithinDistanceHelper(node.leftChild, tuple, k+1, minDist, maxDist)
			list = merge(list, newList) // merge two lists
		}
		if tuple[k]+minDist > loc[k] { // and here, check the right then the left
			newList := tree.findAllNeighborsIndsWithinDistanceHelper(node.rightChild, tuple, k+1, minDist, maxDist)
			list = merge(list, newList) // merge two lists
		}
	}

	return list

}

// FindNearestNeighborTuple returns the closest tuple of the passed point
func (tree *KDTree) FindNearestNeighborTuple(tuple []float64) []float64 {
	idx := tree.FindNearestNeighborInd(tuple)
	foundTuple := tree.Get(idx)
	tupleCopy := make([]float64, len(foundTuple))
	copy(tupleCopy, foundTuple)
	return tupleCopy
}

// FindNearestNeighborInd returns the index for the nearest tuple of the passed point
func (tree *KDTree) FindNearestNeighborInd(tuple []float64) int {
	arr := tree.findNearestNNeighborsIndsHelper(tree.root, tuple, 0, 1, 1, math.MaxFloat64)
	idx := arr[0].idx
	return idx
}

/*UTILITY STUFF*/
func distanceSq(A, B []float64) float64 {
	total := 0.0
	for d := range A {
		diff := A[d] - B[d]
		//total += math.Pow(diff, 2)
		total += (diff * diff)
	}
	return total
}

const epsilon float64 = 0.000001 // tolerance for floating point equality

func equalsWithinTol(a, b, tol float64) bool {
	return math.Abs(a-b) < tol
}

func equals(a, b float64) bool {
	return equalsWithinTol(a, b, epsilon)
}

func tupleEqualsWithinTol(A, B []float64, tol float64) bool {
	if A == nil || B == nil {
		return false
	}
	if len(A) != len(B) {
		return false
	}
	for idx := range A {
		if !equalsWithinTol(A[idx], B[idx], tol) {
			return false
		}
	}
	return true

}

func tupleEquals(A, B []float64) bool {
	return tupleEqualsWithinTol(A, B, epsilon)
}

/*END UTILITY */
